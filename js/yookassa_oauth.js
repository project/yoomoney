(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.oauthBehavior = {
        attach: function (context, settings) {
            const buttonOauthConnect =  $(once('qa-oauth-info', 'button.btn_oauth_connect', context));

            /**
             * Событие на кнопки Подключить магазин и Сменить магазин
             */
            buttonOauthConnect.click(function (e) {
                e.preventDefault();
                changeButton(true);
                const idField = $('input[name="id"]');
                const machineName = idField !== undefined ? idField.val() : $('span.machine-name-value').html();
                if ($('.btn_oauth_connect', context).hasClass('new_record')) {
                    checkPaymentMethod(machineName);
                } else {
                    fetchOauthLink(machineName);
                }
            });

            /**
             * Запрос на бэк для получения ссылки на авторизацию в OAuth
             * @param machineName - ID платежного шлюза
             */
            function fetchOauthLink(machineName) {
                const form = $('form').serialize();
                $.get(Drupal.url('get_oauth_url'),
                    {name: machineName, form: form},
                    function (response) {
                        if (response.oauth_url != null) {
                            showOauthWindow(response.oauth_url, machineName);
                        } else {
                            addErrorBlock();
                        }
                    })
                    .fail(function (jqXHR, textStatus, error) {
                        addErrorBlock();
                        if (typeof jqXHR.responseJSON == "undefined") {
                            console.error(jqXHR, textStatus, error);
                            return;
                        }
                        console.error(jqXHR.responseJSON.error, textStatus, error);
                    });
            }

            /**
             * Показ окна с авторизацией в OAuth
             * @param url - Ссылка в OAuth
             * @param machineName - ID платежного шлюза
             */
            function showOauthWindow(url, machineName) {
                const oauthWindow = window.open(
                    url,
                    'Авторизация',
                    'width=600,height=600, top=' + ((screen.height - 600) / 2) + ', left=' + ((screen.width - 600) / 2 + window.screenLeft) + ', menubar=no, toolbar=no, location=no, resizable=yes, scrollbars=no, status=yes');

                const timer = setInterval(function () {
                    if (oauthWindow.closed) {
                        if (oauthWindow.closed) {
                            clearInterval(timer);
                            getOauthToken(machineName);
                        }
                    }
                }, 1000);
            }

            /**
             * Инициализация получения OAuth токена
             */
            function getOauthToken(machineName) {
                const notifyUrlVal = $('input[name="configuration[yookassa][column][notification_url]"]').val();
                $.get(Drupal.url('get_oauth_token'),
                    {name: machineName, notifyUrl: notifyUrlVal},
                    function (response) {
                        if (response.url !== undefined) {
                            window.location = location.origin + response.url;
                        } else {
                            addErrorBlock();
                        }
                    })
                    .fail(function (jqXHR, textStatus, error) {
                        addErrorBlock();
                        if (typeof jqXHR.responseJSON == "undefined") {
                            console.error(jqXHR, textStatus, error);
                            return;
                        }
                        console.error(jqXHR.responseJSON.error, textStatus, error);
                    });
            }

            /**
             * Меняет состояние и текст кнопки
             * @param isDisabled - включение\отключение disabled режима кнопки
             * @param buttonText - текст для кнопки
             * @param isSpinner - включение\отключение режима прелоадера
             */
            function changeButton(isDisabled, buttonText = null, isSpinner = true) {
                const button = $('.btn_oauth_connect', context);
                button.attr('disabled', isDisabled);
                button.html(isSpinner === true ? '<span class="spinner qa-spinner"></span>' : buttonText);
            }

            /**
             * Добавляет блок с ошибкой при неуспешной oauth авторизации
             */
            function addErrorBlock() {
                const blockOauth = $('.oauth_info');
                const errorBlock = '<div class="oauth-error">' + Drupal.t('Something went wrong. Refresh the page and try again.') + '</div>';
                blockOauth.after(errorBlock);
            }

            /**
             * Добавляет блок с ошибкой при неуспешной валидации на название платежного шлюза
             * @param message - текст сообщения
             */
            function addValidErrorBlock(message) {
                const formBlock = $('form');
                const errorBlock = '<div class="messages messages--error yookassa">' + message + '</div>';
                formBlock.before(errorBlock);
            }

            /**
             * Проверка на существование платежного шлюза по машинному имени
             * @param machineName - ID платежного шлюза
             */
            function checkPaymentMethod(machineName) {
                const errorBlock = $('.messages.messages--error.yookassa');
                if (errorBlock !== undefined) {
                    errorBlock.remove();
                }
                $.get(Drupal.url('check_payment_method'),
                    {name: machineName},
                    function (response) {
                        if (response.error !== null) {
                            addValidErrorBlock(response.error);
                            changeButton(false, Drupal.t('Connect your store'), false);
                        }

                        if (response.error === null) {
                            beginningAuthorization(machineName);
                        }
                    })
                    .fail(function (jqXHR, textStatus, error) {
                        addErrorBlock();
                        if (typeof jqXHR.responseJSON == "undefined") {
                            console.error(jqXHR, textStatus, error);
                            return;
                        }
                        console.error(jqXHR.responseJSON.error, textStatus, error);
                    });
            }

            /**
             * Генерирует URL для уведомлений и запускает процесс авторизации
             * @param machineName - ID платежного шлюза
             */
            function beginningAuthorization(machineName) {
                const notifyUrl = $('input[name="configuration[yookassa][column][notification_url]"]');
                $.get(Drupal.url('generate_notification_url'),
                    {name: machineName},
                    function (response) {
                        if (response.url !== undefined) {
                            notifyUrl.val(response.url);
                            fetchOauthLink(machineName);
                        } else {
                            addErrorBlock();
                        }
                    })
                    .fail(function (jqXHR, textStatus, error) {
                        addErrorBlock();
                        if (typeof jqXHR.responseJSON == "undefined") {
                            console.error(jqXHR, textStatus, error);
                            return;
                        }
                        console.error(jqXHR.responseJSON.error, textStatus, error);
                    });
            }
        }
    }
})(jQuery, Drupal, drupalSettings);
