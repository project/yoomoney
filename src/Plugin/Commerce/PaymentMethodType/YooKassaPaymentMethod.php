<?php

/**
 * @author YooMoney <cms@yoomoney.ru>
 * @copyright © 2025 "YooMoney", NBСO LLC
 * @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Абстрактный класс для платежных методов
 */
abstract class YooKassaPaymentMethod extends PaymentMethodTypeBase
{
    /**
     * {@inheritdoc}
     */
    abstract public function buildLabel(PaymentMethodInterface $payment_method): string;

    /**
     * {@inheritdoc}
     */
    public function buildFieldDefinitions(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getCreateLabel(): string
    {
        return $this->getLabel();
    }
}