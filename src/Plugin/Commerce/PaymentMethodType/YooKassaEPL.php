<?php

/**
 * @author YooMoney <cms@yoomoney.ru>
 * @copyright © 2025 "YooMoney", NBСO LLC
 * @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Annotation\CommercePaymentMethodType;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Класс для метода оплаты через EPL
 *
 * @CommercePaymentMethodType(
 *   id = "yookassa_epl",
 *   label = @Translation("YooMoney (bank cards, e-money, etc.)"),
 *   create_label = @Translation("YooMoney (bank cards, e-money, etc.)"),
 * )
 */
class YooKassaEPL extends YooKassaPaymentMethod
{
    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return $this->t('YooMoney (bank cards, e-money, etc.)');
    }

    /**
     * {@inheritdoc}
     */
    public function buildLabel(PaymentMethodInterface $payment_method): string
    {
        return $this->getLabel();
    }
}