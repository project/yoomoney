<?php

/**
 * @author YooMoney <cms@yoomoney.ru>
 * @copyright © 2025 "YooMoney", NBСO LLC
 * @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\Oauth;

use Drupal;
use Drupal\yookassa\Plugin\Commerce\PaymentGateway\YooKassa;
use YooKassa\Client;

/**
 * Фабрика для получения единственного статического экземпляра клиента API Юkassa
 */
class YooKassaClientFactory
{
    /** @var string Полученный токен */
    const YOOKASSA_ACCESS_TOKEN_KEY = 'access_token';

    /** @var string Название CMS */
    const CMS_NAME = 'drupal';

    /** @var string Название модуля */
    const MODULE_NAME = 'yoo_api_drupal10';

    /** @var Client|null Класс клиента API */
    private static ?Client $client = null;

    /**
     * Возвращает единственный инстанс клиента API Юkassa.
     *
     * @param array $config Конфигурация платежного шлюза
     *
     * @return Client
     */
    public static function getYooKassaClient(array $config): Client
    {
        if (!self::$client) {
            self::$client = self::getClient($config);
        }

        return self::$client;
    }

    /**
     * Возвращает объект клиента API Юkassa.
     *
     * @param array $config Конфигурация платежного шлюза
     *
     * @return Client
     */
    private static function getClient(array $config): Client
    {
        $apiClient = new Client();

        $oauthToken = $config[self::YOOKASSA_ACCESS_TOKEN_KEY] ?? null;

        if ($oauthToken) {
            $apiClient->setAuthToken($oauthToken);
            self::setApiClientData($apiClient);

            return $apiClient;
        }

        self::setApiClientData($apiClient);
        return $apiClient;
    }

    /**
     * Устанавливает значение свойств для user-agent.
     *
     * @param Client $apiClient Класс клиента API
     *
     * @return void
     */
    private static function setApiClientData(Client $apiClient): void
    {
        $userAgent = $apiClient->getApiClient()->getUserAgent();
        $apiClient->setLogger(Drupal::logger('yookassa'));
        $userAgent->setCms(self::CMS_NAME, Drupal::VERSION);
        $userAgent->setModule(self::MODULE_NAME, YooKassa::YOOMONEY_MODULE_VERSION);
    }
}
