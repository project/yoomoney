<?php

/**
 * @author YooMoney <cms@yoomoney.ru>
 * @copyright © 2025 "YooMoney", NBСO LLC
 * @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\Oauth;

use Drupal\Core\Config\Config;
use YooKassa\Client;
use YooKassa\Model\Notification\NotificationEventType;

/**
 * Класс методов для работы с oauth вебхуками
 */
class YooKassaWebhookSubscriber
{
    /**
     * Проверяет существующие подписки, удаляет некорректные и создает новые.
     *
     * @param Client $client Класс клиента API
     * @param array $config Конфигурация платежного шлюза
     *
     * @return void
     */
    public static function subscribe(Client $client, array $config): void
    {
        $needWebHookList = [
            NotificationEventType::PAYMENT_SUCCEEDED,
            NotificationEventType::PAYMENT_CANCELED,
            NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE,
            NotificationEventType::REFUND_SUCCEEDED,
        ];

        $webHookUrl = $config['notification_url'];

        $currentWebHookList = $client->getWebhooks()->getItems();
        foreach ($needWebHookList as $event) {
            $hookIsSet = false;
            foreach ($currentWebHookList as $webHook) {
                if ($webHook->getEvent() === $event) {
                    if ($webHook->getUrl() === $webHookUrl) {
                        $hookIsSet = true;
                        continue;
                    }

                    $client->removeWebhook($webHook->getId());
                }
            }
            if (!$hookIsSet) {
                $client->addWebhook(['event' => $event, 'url' => $webHookUrl]);
            }
        }
    }
}
