<?php

/**
 * @author YooMoney <cms@yoomoney.ru>
 * @copyright © 2025 "YooMoney", NBСO LLC
 * @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\Oauth;

use Drupal;
use Drupal\Core\Config\Config;
use Exception;

/**
 * Класс для работы с oauth
 */
class YooKassaOauth
{
    /** @var string Путь до oauth приложения */
    const OAUTH_CMS_URL = 'https://yookassa.ru/integration/oauth-cms';

    /** @var string Часть роута для формирования ссылки на авторизацию мерчаната в OAuth */
    const AUTHORIZATION = 'authorization';

    /** @var string Часть роута для получения OAuth токена */
    const GET_TOKEN = 'get-token';

    /** @var string Часть роута для отзыва OAuth токена */
    const REVOKE_TOKEN = 'revoke-token';

    /** @var string Название CMS */
    const CMS_NAME = 'drupal';

    /** @var Config Конфигурация платежного шлюза, которую можно отредактировать и сохранить в БД */
    public Config $paymentMethodEditConfig;

    /** @var array Конфигурация платежного шлюза преобразованная в массив */
    public array $arrayConfig;

    /** @var string Уникальный id для запросов в OAuth приложение */
    public string $state;

    /**
     * Конструктор YooKassaOauth.
     *
     * @param string|null $machineName Машинное имя платежного шлюза
     */
    public function __construct(?string $machineName)
    {
        $this->paymentMethodEditConfig = Drupal::configFactory()->getEditable('commerce_payment.commerce_payment_gateway.' . $machineName);
        $this->arrayConfig = $this->paymentMethodEditConfig->getOriginal('configuration');
        $this->state = $this->getOauthState();
    }

    /**
     * Формирование данных и отправка запроса на получение ссылки для авторизации.
     *
     * @return array
     * @throws Exception
     */
    public function generateOauthUrl(): array
    {
        $parameters = [
            'state' => $this->state,
            'cms' => self::CMS_NAME,
            'host' => $_SERVER['HTTP_HOST']
        ];

        $this->log('Sending request for OAuth link. Request parameters: ' . json_encode($parameters));

        return $this->sendRequest(self::AUTHORIZATION, $parameters);
    }

    /**
     * Формирование данных и отправка запроса на получение токена авторизации.
     *
     * @return array
     * @throws Exception
     */
    public function generateOauthToken(): array
    {
        $parameters = ['state' => $this->state];

        $this->log('Sending request for OAuth token. Request parameters: ' . json_encode($parameters));

        return $this->sendRequest(self::GET_TOKEN, $parameters);
    }

    /**
     * Проверяет в БД state и возвращает его, если нет в БД, генерирует его.
     *
     * @return string
     */
    public function getOauthState(): string
    {
        $state = !empty($this->arrayConfig['oauth_state']) ? $this->arrayConfig['oauth_state'] : null;

        if (!$state) {
            $state = substr(md5(time()), 0, 12);
            $this->saveConfigurationPayment(['configuration.oauth_state' => $state]);
        }

        return $state;
    }

    /**
     * Выполняет запрос в OAuth приложение на отзыв токена.
     *
     * @param string $token Токен
     *
     * @return void
     * @throws Exception
     */
    public function revokeOldToken(string $token): void
    {
        $parameters = [
            'state' => $this->state,
            'token' => $token,
            'cms' => self::CMS_NAME
        ];

        $this->log('Sending request to revoke OAuth token. Request parameters: ' . json_encode($parameters));

        $result = $this->sendRequest(self::REVOKE_TOKEN, $parameters);

        $body = json_decode($result['response'], true);

        if (!isset($body['success'])) {
            $this->log(
                'Got error while revoking OAuth token. Response body: '
                . json_encode($body), 'error'
            );
        }
    }

    /**
     * Отправка POST запроса и получение данных.
     *
     * @param string $url Url по которому будет произведен запрос
     * @param array $parameters Параметры с которыми будет произведен запрос
     *
     * @return array
     * @throws Exception
     */
    private static function sendRequest(string $url, array $parameters): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::OAUTH_CMS_URL . '/' . $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type' => 'application/json; charset=utf-8']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parameters));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($response === false) {
            throw new Exception('Exception: ' . curl_errno($ch) . ": " . curl_error($ch));
        }

        return ['code' => $info['http_code'], 'response' => $response];
    }

    /**
     * Сохраняет информацию о магазине.
     *
     * @return void
     * @throws Exception
     */
    public function saveShopInfoByOauth(): void
    {
        $apiClient = YooKassaClientFactory::getYooKassaClient($this->arrayConfig);
        $shopInfo = $apiClient->me();

        if (!isset($shopInfo['account_id'])) {
            throw new Exception('Failed to save shop info');
        }

        $this->saveConfigurationPayment([
            'configuration.shop_id' => $shopInfo['account_id'],
        ]);
    }

    /**
     * Создает запись в логе.
     *
     * @param string $message Сообщение
     * @param string $type Тип сообщение
     *
     * @return void
     */
    private static function log(string $message, string $type = 'info')
    {
        Drupal::logger('yookassa')->$type($message);
    }

    /**
     * Запись полей конфигурации платежного шлюза.
     *
     * @param array $configs Массив с параметрами, которые необходимо сохранить
     *
     * @return void
     */
    public function saveConfigurationPayment(array $configs): void
    {
        foreach ($configs as $key => $value) {
            $this->paymentMethodEditConfig->set($key, $value);
        }
        $this->paymentMethodEditConfig->save(true);
    }
}
