<?php

/**
 * @author YooMoney <cms@yoomoney.ru>
 * @copyright © 2025 "YooMoney", NBСO LLC
 * @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\EventSubscriber;

use Drupal;
use Drupal\commerce_log\LogStorageInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\yookassa\Helpers\YooKassaLudwigRequireHelper;
use Drupal\yookassa\Oauth\YooKassaClientFactory;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use YooKassa\Client;
use YooKassa\Common\AbstractObject;
use YooKassa\Common\Exceptions\ApiException;
use YooKassa\Common\Exceptions\BadApiRequestException;
use YooKassa\Common\Exceptions\ExtensionNotFoundException;
use YooKassa\Common\Exceptions\ForbiddenException;
use YooKassa\Common\Exceptions\InternalServerError;
use YooKassa\Common\Exceptions\NotFoundException;
use YooKassa\Common\Exceptions\ResponseProcessingException;
use YooKassa\Common\Exceptions\TooManyRequestsException;
use YooKassa\Common\Exceptions\UnauthorizedException;
use YooKassa\Common\ListObjectInterface;
use YooKassa\Model\Receipt\PaymentMode;
use YooKassa\Model\Receipt\ReceiptCustomer;
use YooKassa\Model\Receipt\ReceiptItem;
use YooKassa\Model\Receipt\ReceiptType;
use YooKassa\Model\Receipt\Settlement;
use YooKassa\Request\Receipts\CreatePostReceiptRequest;
use YooKassa\Request\Receipts\PaymentReceiptResponse;
use YooKassa\Request\Receipts\ReceiptResponseInterface;

/**
 * Класс подписки на событие предсохранения заказа для отправки второго чека
 */
class YooKassaEventSubscriber implements EventSubscriberInterface
{
    /** @var EntityTypeManagerInterface Менеджер сущностей */
    protected EntityTypeManagerInterface $entityTypeManager;

    /** @var ModuleHandlerInterface Менеджер обработчика модуля */
    protected ModuleHandlerInterface $moduleHandler;

    /** @var Client Класс клиента API */
    public Client $apiClient;

    /** @var array|null Конфигурация платежного шлюза */
    protected ?array $config;

    /**
     * Конструктор YooKassaEventSubscriber.
     *
     * @param EntityTypeManagerInterface $entity_type_manager
     * @param ModuleHandlerInterface $module_handler
     */
    public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler)
    {
        $this->entityTypeManager = $entity_type_manager;
        $this->moduleHandler = $module_handler;
    }

    /**
     * Список событий на которые необходимо подписаться и метод-триггер
     *
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'commerce_order.commerce_order.presave' => 'onSendSecondReceipt'
        ];
    }

    /**
     * Создает запись в логе.
     *
     * @param string $message Сообщение
     * @param string $type Тип сообщение
     *
     * @return void
     */
    private function log(string $message, string $type)
    {
        Drupal::logger('yookassa')->$type($message);
    }

    /**
     * Отправляет второй чек.
     *
     * @param OrderEvent $event Класс с событиями для заказов
     *
     * @return void|null
     * @throws ApiException
     * @throws BadApiRequestException
     * @throws Drupal\Core\Entity\EntityStorageException
     * @throws ExtensionNotFoundException
     * @throws ForbiddenException
     * @throws InternalServerError
     * @throws InvalidPluginDefinitionException
     * @throws NotFoundException
     * @throws PluginNotFoundException
     * @throws ResponseProcessingException
     * @throws TooManyRequestsException
     * @throws UnauthorizedException|MissingDataException
     */
    public function onSendSecondReceipt(OrderEvent $event)
    {
        YooKassaLudwigRequireHelper::checkLudwigRequire();
        /** @var Order $order */
        $order = $event->getOrder();
        $isSentSecondReceipt = $order->getData('yookassa_send_second_receipt') ?? false;
        $state = $order->getState()->getValue();
        $this->config = $this->getPaymentConfig($order);

        if (
            $isSentSecondReceipt
            || empty($this->config['second_receipt_status'])
            || empty($this->config['second_receipt_enabled'])
        ) {
            return;
        }

        if (
            !$this->config['second_receipt_enabled']
            && $state['value'] !== $this->config['second_receipt_status']
        ) {
            return;
        }

        $this->apiClient = YooKassaClientFactory::getYooKassaClient($this->config);
        $orderId = $order->get('order_id')->getString();
        $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadByProperties(['order_id' => $orderId]);
        $paymentId = !empty($payments) ? array_shift($payments)->getRemoteId() : null;
        if (!$paymentId) {
            return;
        }
        $amount = $order->getTotalPrice()->getNumber();
        $customerEmail = $order->get('mail')->getString();

        /** @var ReceiptResponseInterface $lastReceipt */
        if (!$lastReceipt = $this->getLastReceipt($paymentId)) {
            $this->log('LastReceipt is empty!', 'error');
            return;
        }

        if ($receiptRequest = $this->buildSecondReceipt($lastReceipt, $paymentId, $customerEmail)) {
            try {
                $this->apiClient->createReceipt($receiptRequest);
                $order->setData('yookassa_send_second_receipt', true);
                $this->log('SecondReceipt Send: ' . json_encode($receiptRequest), 'info');

                if ($this->moduleHandler->moduleExists('commerce_log')) {
                    /** @var LogStorageInterface $logStorage */
                    $logStorage = $this->entityTypeManager->getStorage('commerce_log');
                    $logStorage->generate($order, 'order_sent_second_reciept', ['amount' => number_format((float)$amount, 2, '.', '')])->save();
                }
            } catch (ApiException $e) {
                $this->log('SecondReceipt Error: ' . json_encode([
                        'error' => $e->getMessage(),
                        'request' => $receiptRequest->toArray(),
                        'lastReceipt' => $lastReceipt->toArray(),
                        'paymentId' => $paymentId,
                        'customEmail' => $customerEmail
                    ]), 'error');
            }
        }
    }

    /**
     * Возвращает данные о последнем чеке.
     *
     * @param string $paymentId Id платежа
     *
     * @return AbstractObject|null
     */
    private function getLastReceipt(string $paymentId): ?AbstractObject
    {
        try {
            /** @var ListObjectInterface $receipts */
            $receipts = $this->apiClient->getReceipts([
                'payment_id' => $paymentId,
            ])->getItems();
        } catch (Exception $e) {
            $this->log('Fail get last receipt: ' . json_encode([
                    'error' => $e->getMessage(),
                    'paymentId' => $paymentId,
                ]), 'error');
            return null;
        }

        if ($receipts->count() === 0) {
            return null;
        }

        return $receipts->get(0);
    }

    /**
     * Формирует второй чек.
     *
     * @param ReceiptResponseInterface $lastReceipt Объект последнего чека
     * @param string $paymentId Id платежа
     * @param string $customerEmail Email пользователя
     *
     * @return void|CreatePostReceiptRequest
     */
    private function buildSecondReceipt(ReceiptResponseInterface $lastReceipt, string $paymentId, string $customerEmail)
    {
        if ($lastReceipt->getType() === "refund") {
            $this->log('Last receipt is refund', 'error');
            return null;
        }

        $resendItems = $this->getResendItems($lastReceipt->getItems()->toArray());

        if (!count($resendItems['items'])) {
            $this->log('Second receipt is not required', 'error');
            return null;
        }

        try {
            $customer = new ReceiptCustomer();
            $customer->setEmail($customerEmail);

            if (empty($customer)) {
                $this->log('Customer email for second receipt are empty', 'error');
                return null;
            }

            $settlement = new Settlement([
                'type' => 'prepayment',
                'amount' => [
                    'value' => $resendItems['amount'],
                    'currency' => 'RUB',
                ],
            ]);
            $receiptBuilder = CreatePostReceiptRequest::builder();
            $receiptBuilder->setObjectId($paymentId)
                ->setType(ReceiptType::PAYMENT)
                ->setItems($resendItems['items'])
                ->setSettlements([$settlement])
                ->setCustomer($customer)
                ->setSend(true)
                ->setTaxSystemCode($lastReceipt->getTaxSystemCode());

            return $receiptBuilder->build();

        } catch (Exception $e) {
            $this->log('Build second receipt error: ' . json_encode([
                    'message' => $e->getMessage()
                ]), 'error');
        }

        return null;
    }

    /**
     * Обработка товаров и заполнение необходимыми данными.
     *
     * @param array $items Товары в чеке
     *
     * @return array
     */
    private function getResendItems(array $items): array
    {
        $resendItems = [
            'items'  => [],
            'amount' => 0,
        ];

        foreach ($items as $item) {
            if ($item['payment_mode'] === PaymentMode::FULL_PREPAYMENT) {
                $item['payment_mode'] = PaymentMode::FULL_PAYMENT;
                $resendItems['items'][] = new ReceiptItem($item);
                $resendItems['amount'] += $item['amount']['value'] * $item['quantity'];
            }
        }

        return $resendItems;
    }

    /**
     * Получение конфигурации платежного шлюза из данных заказа.
     *
     * @param Order $order Модель заказа
     *
     * @return mixed|null
     */
    private function getPaymentConfig(Order $order): mixed
    {
        $machineName = $order->get('payment_gateway')->getString();

        return $machineName ? Drupal::config('commerce_payment.commerce_payment_gateway.' . $machineName)->getOriginal('configuration') : null;
    }
}
