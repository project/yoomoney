<?php

/**
 * @author YooMoney <cms@yoomoney.ru>
 * @copyright © 2025 "YooMoney", NBСO LLC
 * @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\Helpers;

use Drupal;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Core\Entity\EntityStorageException;
use Exception;

/**
 * Класс методов для проверки и подготовки данных по работе с платежными шлюзами
 */
class YooKassaPaymentMethodHelper
{
    /**
     * Проверка на существование записи платежного шлюза.
     *
     * @param string $machineName Машинное имя платежного шлюза
     *
     * @return bool
     */
    public static function check(string $machineName): bool
    {
        $paymentMethod = Drupal::configFactory()->getEditable('commerce_payment.commerce_payment_gateway.' . $machineName)->getOriginal('configuration');
        return (bool)$paymentMethod;
    }

    /**
     * Подготовка массива для создания нового платежного шлюза.
     *
     * @param array $formData
     * @param string $machineName Машинное имя платежного шлюза
     * @return array
     */
    public static function prepareArray(array $formData, string $machineName): array
    {
        $result = [
            'id' => $machineName,
            'label' => $formData['label'],
            'plugin' => 'yookassa',
            'status' => 0,
            'dependencies' => [
                'module' => 'yookassa'
            ]
        ];

        $result['configuration']['display_label'] = $formData['configuration']['yookassa']['display_label'];

        foreach ($formData['configuration']['yookassa']['column'] as $name => $value) {
            $result['configuration'][$name] = $value;
        }

        return $result;
    }

    /**
     * Подготовка данных и сохранение нового платежного шлюза.
     *
     * @param string $formData Данные с формы
     * @param string $machineName Машинное имя платежного шлюза
     *
     * @return void
     * @throws EntityStorageException
     * @throws Exception
     */
    public static function savePaymentMethod(string $formData, string $machineName): void
    {
        if (empty($formData)) {
            throw new Exception('Form data is empty');
        }
        $data = self::parsingFormData($formData);

        if (empty($data)) {
            throw new Exception('Form data parsing error');
        }
        $paymentArray = self::prepareArray($data, $machineName);
        self::saveMethod($paymentArray);
    }

    /**
     * Парсинг данных формы.
     *
     * @param string $formData Данные с формы
     *
     * @return array
     */
    public static function parsingFormData(string $formData): array
    {
        $result = urldecode($formData);
        parse_str($result, $form);
        return $form;
    }

    /**
     * Сохранение в БД нового платежного шлюза.
     *
     * @param array $paymentArray Массив данных, которые необходимо сохранить
     *
     * @return void
     * @throws EntityStorageException
     */
    public static function saveMethod(array $paymentArray)
    {
        $payment_gateway = PaymentGateway::create($paymentArray);
        $payment_gateway->setPluginConfiguration($paymentArray['configuration']);
        $payment_gateway->save();
    }
}
