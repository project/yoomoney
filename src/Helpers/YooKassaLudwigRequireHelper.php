<?php

/**
 * @author YooMoney <cms@yoomoney.ru>
 * @copyright © 2025 "YooMoney", NBСO LLC
 * @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\Helpers;

use Drupal;

/**
 * Класс методов для работы с Ludwig
 */
class YooKassaLudwigRequireHelper
{
    /**
     * Проверяем, подключен ли пакет Ludwig и существует ли нужная версия библиотеки,
     * если да, то подключаем библиотеку
     *
     * @return void
     */
    public static function checkLudwigRequire(): void
    {
        $dir_name = dirname(__FILE__, 2);
        $package_name = 'yoomoney/yookassa-sdk-php';
        $file_to_require = 'lib/autoload.php';
        $ludwig_json = $dir_name . '/ludwig.json';

        if (!file_exists($ludwig_json)) {
            return;
        }
        $packages = file_get_contents($ludwig_json);
        $packages = json_decode($packages, TRUE);
        $version = $packages['require'][$package_name]['version'];
        $require = $dir_name . '/lib/' . str_replace('/', '-', $package_name) . '/' . $version . '/' . $file_to_require;

        if (Drupal::hasService('ludwig.require_once') && file_exists($require)) {
            $ludwig_require_once = Drupal::service('ludwig.require_once');
            $ludwig_require_once->requireOnce($package_name, $file_to_require, $dir_name);
        }
    }
}
