<?php

/**
 *  @author YooMoney <cms@yoomoney.ru>
 *  @copyright © 2025 "YooMoney", NBСO LLC
 *  @license  https://yoomoney.ru/doc.xml?id=527052
 */

namespace Drupal\yookassa\Controller;

use Drupal;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\yookassa\Helpers\YooKassaPaymentMethodHelper;
use Drupal\yookassa\Plugin\Commerce\PaymentGateway\YooKassa;
use Exception;
use Drupal\yookassa\Oauth\YooKassaClientFactory;
use Drupal\yookassa\Oauth\YooKassaWebhookSubscriber;
use Drupal\yookassa\Oauth\YooKassaOauth;
use Symfony\Component\HttpFoundation\Request;

/**
 * Контроллер для работы с oauth
 */
class YooKassaOauthController extends ControllerBase
{
    /**
     * Делает запрос к OAuth приложению для получения ссылки на авторизацию.
     *
     * @param Request $request Модель запроса
     *
     * @return AjaxResponse
     */
    public function getOauthUrl(Request $request): AjaxResponse
    {
        $machineName = $request->get('name') ?: null;
        $formData = $request->get('form') ?: null;

        $response = new AjaxResponse();
        $is_ajax = $request->isXmlHttpRequest();

        if (!$is_ajax) {
            $response->setData(['status' => 'error', 'error' => 'Unknown', 'code' => 'unknown']);
            return $response;
        }

        try {
            if (!YooKassaPaymentMethodHelper::check($machineName)) {
                YooKassaPaymentMethodHelper::savePaymentMethod($formData, $machineName);
            }

            $oauth = new YooKassaOauth($machineName);
            $result = $oauth->generateOauthUrl();
        } catch (Exception $e) {
            $this->errorExit('Exception: ' . $e->getMessage(), $e->getMessage(), 500);
        }
        $code = $result['code'];

        if ($code != 200) {
            $this->errorExit('Got error while getting OAuth link.');
        }

        $body = json_decode($result['response'], true);

        if (!isset($body['oauth_url'])) {
            $error = empty($body['error']) ? 'OAuth URL not found' : $body['error'];
            $this->errorExit('Got error while getting OAuth link. Response body: ' . json_encode($body), $error);
        }

        $response->setData(['oauth_url' => $body['oauth_url']]);
        return $response;
    }

    /**
     * Функция обработки ajax запроса на получение OAuth токена через OAuth-приложение.
     *
     * @param Request $request Модель запроса
     *
     * @return AjaxResponse
     * @throws Exception
     */
    public function getOauthToken(Request $request): AjaxResponse
    {
        $machineName = $request->get('name') ?: null;
        $notifyUrl = $request->get('notifyUrl') ?: null;

        $response = new AjaxResponse();
        $is_ajax = $request->isXmlHttpRequest();

        if (!$is_ajax) {
            $response->setData(['status' => 'error', 'error' => 'Unknown', 'code' => 'unknown']);
            return $response;
        }

        $oauth = new YooKassaOauth($machineName);
        try {
            $result = $oauth->generateOauthToken();
        } catch (Exception $e) {
            $this->errorExit('Exception: ' . $e->getMessage(), $e->getMessage(), 500);
        }

        $code = $result['code'];

        if ($code != 200) {
            if ($code == 422) {
                $error = empty($body['error']) ? 'Access token not found' : $body['error'];
                $this->errorExit($error, 'Авторизация не пройдена');
            }
            $this->errorExit('Got error while getting OAuth token.');
        }

        $body = json_decode($result['response'], true);

        if (!isset($body['access_token'])) {
            $error = empty($body['error']) ? 'Access token not found' : $body['error'];
            $this->errorExit('Got error while getting OAuth token. Key access_token not found. Response body: ' . json_encode($body), $error);
        }

        if (!isset($body['expires_in'])) {
            $error = empty($body['error']) ? 'Expires_in parameter not found' : $body['error'];
            $this->errorExit('Got error while getting OAuth token. Key expires_in not found. Response body: ' . json_encode($body), $error);
        }

        $config = $oauth->paymentMethodEditConfig->getOriginal('configuration');
        $token = !empty($config['access_token']) ? $config['access_token'] : null;

        if ($token) {
            $oauth->revokeOldToken($token);
        }

        $config['access_token'] = $body['access_token'];
        $config['token_expires_in'] = $body['expires_in'];
        $config['notification_url'] = $notifyUrl;

        $oauth->saveConfigurationPayment([
            'configuration.access_token' => $body['access_token'],
            'configuration.token_expires_in' => $body['expires_in'],
            'configuration.notification_url' => $notifyUrl,
            'status' => 1
        ]);

        try {
            $client = YooKassaClientFactory::getYooKassaClient($config);
            YooKassaWebhookSubscriber::subscribe($client, $config);
            $oauth->saveShopInfoByOauth();
        } catch (Exception $e) {
            $this->errorExit('Error occurred during creating webhooks: ' . $e->getMessage(), $e->getMessage(), 500);
        }

        $url = Url::fromRoute('entity.commerce_payment_gateway.edit_form', ['commerce_payment_gateway' => $machineName])->toString();

        $response->setData(['url' => $url]);
        return $response;
    }

    /**
     * Вывод сообщений об ошибках и остановка скрипта.
     *
     * @param string $errorLog Сообщение для записи в лог
     * @param string|null $errorFront Сообщение для вывода на фронт пользователю
     * @param int $code Код статуса
     * @return void
     */
    private function errorExit(string $errorLog, string $errorFront = null, int $code = 502): void
    {
        $errorFront = $errorFront ?: $errorLog;
        $this->log('Error: ' . $errorLog, 'error');
        echo json_encode(['error' => $errorFront]);
        exit($code);
    }

    /**
     * Создает запись в логе.
     *
     * @param string $message Сообщение
     * @param string $type Тип сообщение
     *
     * @return void
     */
    private function log(string $message, string $type = 'info'): void
    {
        Drupal::logger('yookassa')->$type($message);
    }

    /**
     * Проверка на существование записи платежного шлюза в БД по машинному имени.
     *
     * @param Request $request Модель запроса
     *
     * @return AjaxResponse
     */
    public function checkPaymentMethod(Request $request): AjaxResponse
    {
        $machineName = $request->get('name') ?: null;
        $response = new AjaxResponse();
        if (!$machineName) {
            $response->setData([
                'error' => $this->t('The "Machine Name" field can\'t be empty')
            ]);
            return $response;
        }

        $response->setData([
            'error' => YooKassaPaymentMethodHelper::check($machineName)
                ? $this->t('The "@machineName" payment gateway already exists. Please enter a different name', ['@machineName' => $machineName])
                : null
        ]);

        return $response;
    }

    /**
     * Генерирует новый URL для уведомлений с новым машинным именем.
     *
     * @param Request $request Модель запроса
     *
     * @return AjaxResponse
     */
    public function generateNotificationUrl(Request $request): AjaxResponse
    {
        $machineName = $request->get('name') ?: null;
        $response = new AjaxResponse();
        $notificationUrl = YooKassa::generateNotificationUrl($machineName);
        $response->setData(['url' => $notificationUrl]);
        return $response;
    }
}
